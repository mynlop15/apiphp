<?php
class Database{
    // credenciales
    private $host = 'localhost';
    private $db_name = 'test';
    private $username = 'root';
    private $password = '';
    public $conn;

    // conexion con la base de datos
    public function getConnection(){
        $this->conn = null;

        try{
            $this->conn = new PDO('mysql:host=' .$this->host .";dbname=" .$this->db_name, $this->username, $this->password);
            $this->conn->exec("set names utf8");
            $this->conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
        } catch(PDOException $exception){
            echo "Error de conexion: " .$exception->getMessage();
        }
        return $this->conn;
    }
}
?>